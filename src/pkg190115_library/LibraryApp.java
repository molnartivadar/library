package pkg190115_library;

import pkg190115_library.controller.LibraryController;

/**
 *
 * @author molnar_t
 */
public class LibraryApp {

    public static void main(String[] args) {

        new LibraryController();

    }

}
