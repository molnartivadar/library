package pkg190115_library.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LibraryClient extends Thread implements Cloneable{

    public void listAllBook(String sql) {

        try (Socket socket = new Socket("localhost", 1982);
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os, true)) {

            System.out.println("Successfully connected to the server");

            ReaderThread rt = new ReaderThread(socket);
            rt.start();

            pw.println(sql);

            

        } catch (IOException ex) {
            Logger.getLogger(LibraryClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
