package pkg190115_library.client;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderThread extends Thread implements Closeable {

    private boolean running = true;
    private Socket socket;

    public ReaderThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr)) {

            String line;
            while (running && (line = br.readLine()) != null) {
                System.out.println("Response from server:");
                System.out.println(line);
                //br.close();
            }
            System.out.println("kilépett a ReaderThread a while ciklusból");

        } catch (IOException ex) {
            Logger.getLogger(ReaderThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (this.socket != null && !this.socket.isClosed()) {
                try {
                    this.socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(ReaderThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void close() throws IOException {
        this.running = false;
    }

}
