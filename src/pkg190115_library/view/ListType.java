package pkg190115_library.view;

/**
 *
 * @author molnar_t
 */
public enum ListType {
    
    ALL_BOOKS,
    AVAILABLE_BOOKS,
    RENTED_BOOKS,
    EBOOKS
}
