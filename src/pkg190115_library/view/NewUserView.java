package pkg190115_library.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import pkg190115_library.controller.LibraryController;

/**
 *
 * @author molnar_t
 */
public class NewUserView extends JFrame implements DocumentListener {
    
    private final JTextPane tpName = new JTextPane();
    private final JTextPane tpAddress = new JTextPane();
    private final JTextField tfName = new JTextField(20);
    private final JTextField tfAddress = new JTextField(20);
    private final JButton btAdd = new JButton("Add");
    private final JButton btCancel = new JButton("Cancel");
    private LibraryController lc;
    private LibraryView lv;
    
    public NewUserView(LibraryController lc, LibraryView lv) {
        this.lc = lc;
        this.lv = lv;
        init();
    }
    
    private void init() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Create new user");
        setSize(320, 140);
        setResizable(false);
        setLocationRelativeTo(this);
        setLayout(new GridLayout(3, 1));
        
        tfName.getDocument().addDocumentListener(this);
        tfAddress.getDocument().addDocumentListener(this);
        JPanel pnName = new JPanel(new FlowLayout());
        tpName.setText("Name:    ");
        tpName.setBackground(getBackground());
        tpName.setEditable(false);
        pnName.add(tpName);
        pnName.add(tfName);
        add(pnName);
        
        JPanel pnAddress = new JPanel(new FlowLayout());
        tpAddress.setText("Address:");
        tpAddress.setBackground(getBackground());
        tpAddress.setEditable(false);
        pnAddress.add(tpAddress);
        pnAddress.add(tfAddress);
        add(pnAddress);
        
        JPanel pnButtons = new JPanel(new FlowLayout());
        
        btAdd.setEnabled(false);
        btAdd.addActionListener((ActionEvent ae) -> {
            String name = tfName.getText();
            String address = tfAddress.getText();
            lc.addUser(name, address);
            lv.addUserToComboBox(name);
            dispose();
        });
        pnButtons.add(btAdd);
        
        btCancel.addActionListener((ActionEvent ae) -> {
            dispose();
        });
        pnButtons.add(btCancel);
        add(pnButtons);
        
        setVisible(true);
    }
    
    @Override
    public void insertUpdate(DocumentEvent de) {
        if (!tfName.getText().isEmpty() && !tfAddress.getText().isEmpty()) {
            btAdd.setEnabled(true);
        } else {
            btAdd.setEnabled(false);
        }
    }
    
    @Override
    public void removeUpdate(DocumentEvent de) {
        if (!tfName.getText().isEmpty() && !tfAddress.getText().isEmpty()) {
            btAdd.setEnabled(true);
        } else {
            btAdd.setEnabled(false);
        }
    }
    
    @Override
    public void changedUpdate(DocumentEvent de) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
