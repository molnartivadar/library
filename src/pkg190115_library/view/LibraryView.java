package pkg190115_library.view;

import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.*;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import pkg190115_library.controller.LibraryController;

/**
 *
 * @author molnar_t
 */
public class LibraryView extends JFrame {

    private final JRadioButton rbAllBooks = new JRadioButton("All books");
    private final JRadioButton rbAvailableBooks = new JRadioButton("Available books");
    private final JRadioButton rbRentedBooks = new JRadioButton("Rented books");
    private final JRadioButton rbEBooks = new JRadioButton("e-Books");
    private final JCheckBox chSocket = new JCheckBox("Socket");
    private final JButton btSearch = new JButton("Search");
    private final JButton btRent = new JButton("Rent selected book(s)");
    private final JButton btRetrieve = new JButton("Retrieve book");
    private final JButton btAddNewUser = new JButton("Add new user");
    private final JButton btAddEBook = new JButton("Add e-Book");
    private final JTextField tfAuthor = new JTextField(20);
    private final JTextPane tpAuthor = new JTextPane();
    private final JTextField tfBookTitle = new JTextField(20);
    private final JTextPane tpBookTitle = new JTextPane();
    private final JTextPane tpUser = new JTextPane();
    private JComboBox cbUsers;
    private JComboBox cbAuthors;
    private JTextPane tpOr;
    private JTable table;
    private final String[] allBooks = {"ISBN", "Book name", "Author name", "pcs", "Book ID"};
    private final String[] rentedBooks = {"ISBN", "Book name", "Author name", "User ID", "Book ID", "Date from", "Date to"};
    private final String[] eBooks = {"ISBN", "Book name"};
    private final DefaultTableModel tableModel = new DefaultTableModel(allBooks, 0);
    private final DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    private ListType rb = ListType.ALL_BOOKS;
    private final LibraryController lc;
    //ImageIcon icon;// = createImageIcon("images/middle.gif");

    public LibraryView(LibraryController lc) {
        this.lc = lc;
        init();
    }

    private void init() {

        setViewParam();

        JScrollPane jsp = showTable();
        Container filterPane = showFilter();
        Container rentPane = showRent();

        add(jsp);
        add(filterPane);
        add(rentPane);

        setVisible(true);
    }

    private void setViewParam() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Library");
        setSize(900, 300);
        setResizable(false);
        setLocationRelativeTo(this);
        setLayout(new GridLayout(3, 1));
    }

    private JScrollPane showTable() {
        table = new JTable(tableModel);
        JScrollPane jsp = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
        table.setColumnSelectionInterval(1, 1);
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.getTableHeader().setFont(new Font("Verdana", Font.BOLD, 12));
        table.getColumnModel().getColumn(0).setPreferredWidth(80);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(120);
        table.getColumnModel().getColumn(3).setPreferredWidth(10);
        table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(4).setPreferredWidth(30);
        table.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        return jsp;
    }

    private Container showFilter() {
        Container filterPane = new Container();
        filterPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        filterPane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.ipadx = 25;

        ButtonGroup bookTypesGroup = new ButtonGroup();
        bookTypesGroup.add(rbAllBooks);
        rbAllBooks.setSelected(true);
        rbAllBooks.addActionListener((ActionEvent ae) -> {
            rb = ListType.ALL_BOOKS;
            tfAuthor.setVisible(true);
            cbAuthors.setVisible(false);
        });
        c.fill = GridBagConstraints.VERTICAL;
        c.ipady = 0;
        c.gridx = 0;
        c.gridy = 0;
        filterPane.add(rbAllBooks, c);

        bookTypesGroup.add(rbAvailableBooks);
        rbAvailableBooks.addActionListener((ActionEvent ae) -> {
            rb = ListType.AVAILABLE_BOOKS;
            tfAuthor.setVisible(true);
            cbAuthors.setVisible(false);
        });
        c.gridx = 1;
        c.gridy = 0;
        filterPane.add(rbAvailableBooks, c);

        bookTypesGroup.add(rbRentedBooks);
        rbRentedBooks.addActionListener((ActionEvent ae) -> {
            rb = ListType.RENTED_BOOKS;
            tfAuthor.setVisible(true);
            cbAuthors.setVisible(false);
        });
        c.gridx = 2;
        c.gridy = 0;
        filterPane.add(rbRentedBooks, c);

        bookTypesGroup.add(rbEBooks);
        rbEBooks.addActionListener((ActionEvent ae) -> {
            rb = ListType.EBOOKS;
            tfAuthor.setVisible(false);
            cbAuthors.setVisible(true);
            for (int i = table.getRowCount() - 1; i >= 0; i--) {
                tableModel.removeRow(i);
            }
        });
        c.gridx = 3;
        c.gridy = 0;
        filterPane.add(rbEBooks);

        c.gridx = 4;
        c.gridy = 0;
        //filterPane.add(chSocket);

        tpAuthor.setText("Author: ");
        tpAuthor.setBackground(getBackground());
        c.insets = new Insets(0, 5, 0, 0);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        filterPane.add(tpAuthor, c);

        tfAuthor.setVisible(true);
        String[] authors = lc.getAuthorsArray();
        cbAuthors = new JComboBox(authors);
        cbAuthors.setVisible(false);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        filterPane.add(tfAuthor, c);
        filterPane.add(cbAuthors, c);

        tpBookTitle.setText("Book title: ");
        tpBookTitle.setBackground(getBackground());
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        filterPane.add(tpBookTitle, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 2;
        filterPane.add(tfBookTitle, c);

        btSearch.addActionListener((ActionEvent ae) -> {
            String author = tfAuthor.getText();
            String eAuthor = (String) cbAuthors.getSelectedItem();
            String bookTitle = tfBookTitle.getText();
            if (chSocket.isSelected()) {
                lc.socketListAllBooks(author, bookTitle);
            } else {
                switch (rb) {
                    case ALL_BOOKS:
                        lc.listAllBooks(author, bookTitle);
                        setButtons();
                        refreshTable();
                        break;
                    case AVAILABLE_BOOKS:
                        lc.listAvailableBooks(author, bookTitle);
                        setButtons();
                        refreshTable();
                        break;
                    case RENTED_BOOKS:
                        lc.listRentedBooks(author, bookTitle);
                        setButtons();
                        refreshTable();
                        break;
                    case EBOOKS:
                        lc.listEBooks(eAuthor, bookTitle);
                        setButtons();
                        refreshTable();
                        break;
                }
            }
        });

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 3;
        c.gridy = 1;
        c.gridheight = 2;
        c.gridwidth = 1;
        filterPane.add(btSearch, c);

        return filterPane;
    }

    private Container showRent() {
        Container rentPane = new Container();
        rentPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        rentPane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.VERTICAL;

        tpUser.setText("Select user:");
        tpUser.setBackground(getBackground());
        tpUser.setVisible(false);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        rentPane.add(tpUser, c);

        String[] users = lc.getUsersArray();
        cbUsers = new JComboBox(users);
        cbUsers.setVisible(false);

        btRetrieve.addActionListener((ActionEvent ae) -> {
            if (table.getSelectedRow() != -1) {
                String bookID = (String) table.getValueAt(table.getSelectedRow(), 4);
                String dateFrom = (String) table.getValueAt(table.getSelectedRow(), 5);
                lc.retrieveBook(bookID, dateFrom);
                refreshTable();
            }
        });
        btRetrieve.setVisible(false);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 0);
        c.gridx = 0;
        c.gridy = 2;
        rentPane.add(cbUsers, c);
        rentPane.add(btRetrieve, c);
        btRetrieve.setVisible(false);

        btRent.addActionListener((ActionEvent ae) -> {
            if (table.getSelectedRow() != -1) {
                String user = (String) cbUsers.getSelectedItem();
                String bookID = (String) table.getValueAt(table.getSelectedRow(), 4);
                String dateFrom = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss").format(Calendar.getInstance().getTime());
                lc.addRent(user, bookID, dateFrom);
                refreshTable();
            }
        });
        btRent.setVisible(false);

        btAddEBook.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (table.getSelectedRow() != -1) {
                    String eAuthor = (String) table.getValueAt(table.getSelectedRow(), 2);
                    String eISBN = (String) table.getValueAt(table.getSelectedRow(), 0);
                    lc.addEBook(eAuthor, eISBN);
                }
            }
        });

        c.gridx = 1;
        c.gridy = 2;
        rentPane.add(btRent, c);
        rentPane.add(btAddEBook, c);

        tpOr = new JTextPane();
        tpOr.setText(" or ");
        tpOr.setBackground(getBackground());
        tpOr.setVisible(false);
        c.gridx = 2;
        c.gridy = 2;
        rentPane.add(tpOr, c);

        btAddNewUser.addActionListener((ActionEvent ae) -> {
            new NewUserView(lc, this);
        });
        c.gridx = 3;
        c.gridy = 2;
        rentPane.add(btAddNewUser, c);

        return rentPane;
    }

    private void refreshTable() {

        setTableColumns();

        List<String> bookList = lc.getBookList(rb);
        for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }

        switch (rb) {
            case ALL_BOOKS:
            case AVAILABLE_BOOKS:
                for (int i = 0; i < bookList.size(); i++) {
                    String isbn = bookList.get(i);
                    String bookTitle = bookList.get(i + 1);
                    String authorName = bookList.get(i + 2);
                    String pcs = bookList.get(i + 3);
                    String bookID = bookList.get(i + 4);

                    Object[] newRow = {isbn, bookTitle, authorName, pcs, bookID};
                    tableModel.addRow(newRow);
                    i = i + 4;
                }
                break;
            case RENTED_BOOKS:
                for (int i = 0; i < bookList.size(); i++) {
                    String isbn = bookList.get(i);
                    String bookTitle = bookList.get(i + 1);
                    String authorName = bookList.get(i + 2);
                    String userName = bookList.get(i + 3);
                    String bookID = bookList.get(i + 4);
                    String dateFrom = bookList.get(i + 5);
                    String dateTo = bookList.get(i + 6);

                    Object[] newRow = {isbn, bookTitle, authorName, userName, bookID, dateFrom, dateTo};
                    tableModel.addRow(newRow);
                    i = i + 6;
                }
                break;
            case EBOOKS:
                for (int i = 0; i < bookList.size(); i++) {
                    String isbn = bookList.get(i);
                    String bookTitle = bookList.get(i + 1);

                    Object[] newRow = {isbn, bookTitle};
                    tableModel.addRow(newRow);
                    i++;
                }
                break;
        }
    }

    private void setTableColumns() {
        switch (rb) {
            case ALL_BOOKS:
            case AVAILABLE_BOOKS:
                tableModel.setColumnIdentifiers(allBooks);
                table.getColumnModel().getColumn(0).setPreferredWidth(80);
                table.getColumnModel().getColumn(1).setPreferredWidth(200);
                table.getColumnModel().getColumn(2).setPreferredWidth(120);
                table.getColumnModel().getColumn(3).setPreferredWidth(10);
                table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
                table.getColumnModel().getColumn(4).setPreferredWidth(30);
                table.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
                break;
            case RENTED_BOOKS:
                tableModel.setColumnIdentifiers(rentedBooks);
                table.getColumnModel().getColumn(0).setPreferredWidth(65);
                table.getColumnModel().getColumn(1).setPreferredWidth(140);
                table.getColumnModel().getColumn(2).setPreferredWidth(110);
                table.getColumnModel().getColumn(3).setPreferredWidth(10);
                table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
                table.getColumnModel().getColumn(4).setPreferredWidth(30);
                table.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
                table.getColumnModel().getColumn(5).setPreferredWidth(80);
                table.getColumnModel().getColumn(6).setPreferredWidth(80);
                break;
            case EBOOKS:
                tableModel.setColumnIdentifiers(eBooks);
                table.getColumnModel().getColumn(0).setPreferredWidth(5);
                table.getColumnModel().getColumn(1).setPreferredWidth(95);
                break;
        }
    }

    public JTextField getTfAuthor() {
        return tfAuthor;
    }

    public JTextField getTfBookTitle() {
        return tfBookTitle;
    }

    public void addUserToComboBox(String newUser) {
        cbUsers.addItem(newUser);
    }

    public void addAuthorToComboBox(String newAuthor) {
        cbAuthors.addItem(newAuthor);
    }

    private void setButtons() {
        switch (rb) {
            case ALL_BOOKS:
                btRent.setVisible(false);
                tpOr.setVisible(false);
                cbUsers.setVisible(false);
                tpUser.setVisible(false);
                btRetrieve.setVisible(false);
                btAddNewUser.setVisible(true);
                btAddEBook.setVisible(true);
                break;
            case AVAILABLE_BOOKS:
                btRent.setVisible(true);
                cbUsers.setVisible(true);
                tpOr.setVisible(true);
                tpUser.setVisible(true);
                btRetrieve.setVisible(false);
                btAddNewUser.setVisible(true);
                btAddEBook.setVisible(false);
                break;
            case RENTED_BOOKS:
                btRent.setVisible(false);
                tpOr.setVisible(false);
                cbUsers.setVisible(false);
                tpUser.setVisible(false);
                btRetrieve.setVisible(true);
                btAddNewUser.setVisible(false);
                btAddEBook.setVisible(false);
                break;
            case EBOOKS:
                btRent.setVisible(true);
                tpOr.setVisible(true);
                cbUsers.setVisible(true);
                tpUser.setVisible(true);
                btRetrieve.setVisible(false);
                btAddNewUser.setVisible(true);
                btAddEBook.setVisible(false);
                break;
        }
    }

}
