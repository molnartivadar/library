package pkg190115_library.server;

import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pkg190115_library.controller.DBControllerData;

public class ClientServingThread extends Thread {

    private Socket socket;
    private DBControllerData data = new DBControllerData();

    public ClientServingThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        try (
                InputStream is = this.socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                OutputStream os = this.socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os, true);) {

            String sql = br.readLine();

            Connection con = null;
            String url = data.getURL();
            String username = data.getUSERNAME();
            String password = data.getPASSWORD();

            try {
                con = DriverManager.getConnection(url, username, password);
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                int columnCount = rs.getMetaData().getColumnCount();
                List<String> dbData = new ArrayList<>();

                while (rs.next()) {
                    for (int i = 1; i <= columnCount; i++) {
                        dbData.add(rs.getString(rs.getMetaData().getColumnName(i)));
                    }
                }
                pw.print(dbData);
                System.out.println("elküldte a ClienServingThread az adatokat");

            } catch (SQLException ex) {
                Logger.getLogger(ClientServingThread.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClientServingThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(ClientServingThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
