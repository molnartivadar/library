package pkg190115_library.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import pkg190115_library.client.LibraryClient;
import pkg190115_library.model.User;
import pkg190115_library.view.LibraryView;
import pkg190115_library.view.ListType;

/**
 *
 * @author molnar_t
 */
public class LibraryController {

    private List<String> bookList = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private LibraryView view;
    private DBService dbc = new DBService();
    private LibraryClient lc = new LibraryClient();

    public LibraryController() {
        view = new LibraryView(this);
    }

    public void addUser(String name, String address) {
        User user = new User(name, address);
        dbc.addUser(name, address);
    }

    public String[] getUsersArray() {
        String[] userArray = dbc.getUsers();
        return userArray;
    }

    public void listAllBooks(String author, String bookTitle) {
        bookList.clear();
        bookList = dbc.listAllBooks(author, bookTitle);
    }

    public List<String> getBookList(ListType rb) {
        switch (rb) {
            case ALL_BOOKS:
                listAllBooks(view.getTfAuthor().getText(), view.getTfBookTitle().getText());
                break;
            case AVAILABLE_BOOKS:
                listAvailableBooks(view.getTfAuthor().getText(), view.getTfBookTitle().getText());
                break;
            case RENTED_BOOKS:
                listRentedBooks(view.getTfAuthor().getText(), view.getTfBookTitle().getText());
                break;
        }
        return bookList;
    }

    public void addRent(String user, String bID, String dateFrom) {
        int bookID = Integer.valueOf(bID);
        dbc.addRent(user, bookID, dateFrom);
    }

    public void retrieveBook(String bID, String dateFrom) {
        int bookID = Integer.valueOf(bID);
        String dateTo = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss").format(Calendar.getInstance().getTime());
        dbc.retrieveBook(bookID, dateTo, dateFrom);
    }

    public void listAvailableBooks(String author, String bookTitle) {
        bookList.clear();
        bookList = dbc.listAvailableBooks(author, bookTitle);
    }

    public void listRentedBooks(String author, String bookTitle) {
        bookList.clear();
        bookList = dbc.listRentedBooks(author, bookTitle);
    }

    public String[] getAuthorsArray() {
        String[] authorsArray = dbc.getAuthors();
        return authorsArray;
    }

    public void listEBooks(String eAuthor, String bookTitle) {
        bookList.clear();
        bookList = dbc.listEBooks(eAuthor, bookTitle);
    }

    public void addEBook(String eAuthor, String eISBN) {
        if (dbc.addEBook(eAuthor, eISBN) == 1) {
            view.addAuthorToComboBox(eAuthor);
        }

    }

    public void socketListAllBooks(String author, String bookTitle) {
        String sql = "SELECT b.ISBN, b.bookTitle, a.authorName, COUNT(bi.ISBN), bi.bookID FROM books b LEFT JOIN book_author ba ON ba.ISBN = b.ISBN LEFT JOIN authors a ON a.authorID = ba.authorID LEFT JOIN book_instances bi ON bi.ISBN = b.ISBN GROUP BY b.ISBN HAVING a.authorName LIKE '%' AND b.bookTitle LIKE '%';";
        //System.out.println(sql);
        bookList.clear();
        lc.listAllBook(sql);
    }

}
