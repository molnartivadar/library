package pkg190115_library.controller;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

/**
 *
 * @author molnar_t
 */
public class DBService {

    private List<String> dbData = new ArrayList<>();
    DBControllerData data = new DBControllerData();

    public int getNextUserId() {

        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        int id = -1;
        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT MAX(readerID) FROM READERS";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                id = Integer.valueOf(rs.getString(1));
            }

        } catch (SQLException ex) {
            System.out.println("Hiba: " + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return id;
    }

    public void addUser(String name, String address) {

        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "INSERT INTO READERS (readerName, readerAddress) VALUES (?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, address);
            int rs = preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Hiba: " + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String[] getUsers() {
        List<String> userList = new ArrayList<>();

        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT readerID, readerName FROM readers";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            dbData.clear();//új
            while (rs.next()) {
                dbData.add(rs.getString(1));
                dbData.add(rs.getString(2));
                userList.add(rs.getString(2));
            }

        } catch (SQLException ex) {
            System.out.println("Hiba: " + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String[] users = new String[userList.size()];
        for (int i = 0; i < userList.size(); i++) {
            users[i] = userList.get(i);
        }

        return users;
    }

    public List<String> listAllBooks(String author, String bookTitle) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT b.ISBN, b.bookTitle, a.authorName, COUNT(bi.ISBN), bi.bookID\n"
                    + "FROM books b\n"
                    + "LEFT JOIN book_author ba ON ba.ISBN = b.ISBN\n"
                    + "LEFT JOIN authors a ON a.authorID = ba.authorID\n"
                    + "LEFT JOIN book_instances bi ON bi.ISBN = b.ISBN\n"
                    + "GROUP BY b.ISBN\n"
                    + "HAVING a.authorName LIKE ? AND b.bookTitle LIKE ?;";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, '%' + author + '%');
            pst.setString(2, '%' + bookTitle + '%');
            ResultSet rs = pst.executeQuery();

            dbData.clear();
            int columnCount = rs.getMetaData().getColumnCount();
            
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    dbData.add(rs.getString(rs.getMetaData().getColumnName(i)));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return dbData;
    }

    public void addRent(String user, int bookID, String dateFrom) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            int readerID = -1;
            getUsers();
            for (int i = 0; i < dbData.size(); i++) {
                if (dbData.get(i).equals(user)) {
                    readerID = Integer.valueOf(dbData.get(i - 1));
                }
            }

            String sql = "INSERT INTO bookrental (readerID, bookID, dateFrom)\n"
                    + "values (?, ?, ?)";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, readerID);
            pst.setInt(2, bookID);
            pst.setString(3, dateFrom);
            int rs = pst.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void retrieveBook(int bookID, String dateTo, String dateFrom) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "UPDATE bookrental SET dateTo = ? WHERE bookID = ? AND dateFrom = ?;";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, dateTo);
            pst.setInt(2, bookID);
            pst.setString(3, dateFrom);
            int rs = pst.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<String> listAvailableBooks(String author, String bookTitle) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT b.isbn, b.bookTitle, a.authorName, COUNT(b.isbn), bi.bookID "
                    + "FROM book_instances bi "
                    + "LEFT JOIN books b ON b.isbn = bi.isbn "
                    + "LEFT JOIN book_author ba ON ba.isbn = bi.isbn "
                    + "LEFT JOIN authors a ON a.authorID = ba.authorID "
                    + "WHERE bi.bookID NOT IN (SELECT br.bookID FROM bookrental br WHERE br.dateTo is NULL) "
                    + "GROUP BY b.isbn "
                    + "HAVING a.authorName LIKE ? AND b.bookTitle LIKE ?;";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, '%' + author + '%');
            pst.setString(2, '%' + bookTitle + '%');
            ResultSet rs = pst.executeQuery();

            dbData.clear();
            int columnCount = rs.getMetaData().getColumnCount();

            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    dbData.add(rs.getString(rs.getMetaData().getColumnName(i)));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return dbData;
    }

    public List<String> listRentedBooks(String author, String bookTitle) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT b.ISBN, b.bookTitle, a.authorName, br.readerID, bi.bookID, br.dateFrom, br.dateTo\n"
                    + "FROM books b\n"
                    + "LEFT JOIN book_author ba ON ba.ISBN = b.ISBN\n"
                    + "LEFT JOIN authors a ON a.authorID = ba.authorID\n"
                    + "LEFT JOIN book_instances bi ON bi.ISBN = b.ISBN\n"
                    + "LEFT JOIN bookrental br ON br.bookID = bi.bookID\n"
                    + "HAVING a.authorName LIKE ? AND b.bookTitle LIKE ? AND br.dateFrom IS NOT NULL;";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, '%' + author + '%');
            pst.setString(2, '%' + bookTitle + '%');
            ResultSet rs = pst.executeQuery();

            dbData.clear();
            int columnCount = rs.getMetaData().getColumnCount();

            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    dbData.add(rs.getString(rs.getMetaData().getColumnName(i)));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return dbData;
    }

    public String[] getAuthors() {
        List<String> authorList = new ArrayList<>();

        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT authorName "
                    + "FROM authors a "
                    + "LEFT JOIN book_author ba ON ba.authorID = a.authorID "
                    + "LEFT JOIN ebooks eb ON eb.ISBN = ba.ISBN "
                    + "WHERE ba.ISBN IN (SELECT * FROM ebooks) "
                    + "GROUP BY a.authorName;";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                authorList.add(rs.getString(1));
            }

        } catch (SQLException ex) {
            System.out.println("Hiba: " + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String[] authors = new String[authorList.size()];
        for (int i = 0; i < authorList.size(); i++) {
            authors[i] = authorList.get(i);
        }

        return authors;
    }

    public List<String> listEBooks(String eAuthor, String bookTitle) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();

        try {
            con = DriverManager.getConnection(url, username, password);
            String sql = "SELECT eb.isbn, b.bookTitle "
                    + "FROM ebooks eb "
                    + "LEFT JOIN books b ON eb.isbn = b.isbn "
                    + "LEFT JOIN book_author ba ON ba.isbn = b.isbn "
                    + "LEFT JOIN authors a ON a.authorID = ba.authorID "
                    + "WHERE a.authorName LIKE ? AND b.bookTitle LIKE ?;";

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, '%' + eAuthor + '%');
            pst.setString(2, '%' + bookTitle + '%');
            ResultSet rs = pst.executeQuery();

            dbData.clear();
            int columnCount = rs.getMetaData().getColumnCount();

            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    dbData.add(rs.getString(rs.getMetaData().getColumnName(i)));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return dbData;
    }

    public int addEBook(String eAuthor, String eISBN) {
        Connection con = null;
        String url = data.getURL();
        String username = data.getUSERNAME();
        String password = data.getPASSWORD();
        int rs = -1;

        try {
            con = DriverManager.getConnection(url, username, password);

            String sql = "INSERT IGNORE INTO ebooks (isbn) VALUES (?);";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, eISBN);
            rs = pst.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rs;
    }

}
