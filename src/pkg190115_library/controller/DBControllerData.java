package pkg190115_library.controller;

/**
 *
 * @author molnar_t
 */
public class DBControllerData {

    private final static String URL = "jdbc:mysql://localhost/library?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final static String USERNAME = "xxxxx";
    private final static String PASSWORD = "xxxxx";

    public String getURL() {
        return URL;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

}
