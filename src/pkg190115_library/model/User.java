package pkg190115_library.model;

import java.util.ArrayList;
import java.util.List;
import pkg190115_library.controller.DBService;

/**
 *
 * @author molnar_t
 */
public class User {

    private String name;
    private String address;
    private int userID;
    private List<User> users = new ArrayList<>();

    public User(String name, String address) {
        this.name = name;
        this.address = address;
        this.userID = getUserIdFromDB();
        users.add(this);
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getUserID() {
        return userID;
    }

    public List<User> getUsers() {
        return users;
    }

    private int getUserIdFromDB() {
        DBService dbc = new DBService();
        return dbc.getNextUserId() + 1;
    }

}
